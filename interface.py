#!/usr/bin/env python 

from tkinter import *
from feu import *
import les_param
import sys

args = les_param.arguments_script()
rows = les_param.f_rows(args)
columns = les_param.f_columns(args)
cell_size = les_param.f_cell_size(args)
afforestation = les_param.f_afforestation(args)
duration = les_param.f_duration(args)

window = Tk()
window.config(bg='lightyellow')
window.title("Fire Simulation 2K20")
canvas = Canvas(window, width=rows*cell_size, height=rows*cell_size)
canvas.grid(row=1, column=1, columnspan=3, pady=30, padx=30)

##Permet de dire si le bouton est cliqué ou pas (0 ou 1)
bruler = IntVar(master=window)

def quitter():
    sys.exit()

def renew():
    CreerGrille(rows,columns)
    initialisation(grille_de_jeu,0.6)
    MiseajourCanva(grille_de_jeu)



btn_new = Button(window,text="Nouvelle carte",command=renew)
btn_new.grid(row = 0,column = 0)

btn_bruler = Checkbutton(window,text="Mettre le feu !!!",indicatoron=0,variable=bruler)
btn_bruler.grid(row = 1 , column = 0,padx = 10,pady = 10)

btn_quit = Button(window,text="Quitter",command=quitter)
btn_quit.grid(row = 2 , column = 0,padx = 10,pady = 10)


def DessinerRectangle(pos_X,pos_Y,value):
    couleur = getColor(value)
    canvas.create_rectangle(pos_X * cell_size, pos_Y * cell_size, (pos_X + 1) * cell_size,
                            (pos_Y + 1) * cell_size, fill=couleur)



def MiseajourCanva(grille):
    for ligne in range(len(grille)):
        for colonne in range(len(grille[0])):
            if grille[ligne][colonne]:
                DessinerRectangle(colonne, ligne, grille[ligne][colonne])



def clique_evenement(event):
    x1 = int(event.x / cell_size)
    y1 = int(event.y / cell_size)
    if grille_de_jeu[y1][x1] == 1:
        grille_de_jeu[y1][x1] = 2
        MiseajourCanva(grille_de_jeu)

canvas.bind('<Button-1>', clique_evenement)


#maj de la map et du canva
def move():
    if bruler.get() == 1 :
        MajForet(grille_de_jeu)
        MiseajourCanva(grille_de_jeu)
    #Le windows.After permet de ne pas avoir l'erreur de recursion maximum
    # ( merci les potes pour le tuyau )
    window.after(10,move)


grille_de_jeu = CreerGrille(rows,columns)
initialisation(grille_de_jeu,0.7)
MiseajourCanva(grille_de_jeu)
move()

window.mainloop()

